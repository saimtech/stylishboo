<?php
// Heading
$_['heading_title']    = 'Zemez Category';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Успешно: Вы изменили настройки модуля Zemez category!';
$_['text_edit']        = 'Редактировать модуль Zemez Category';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_name']       = 'Название модуля';

// Error
$_['error_permission'] = 'У вас нет разрешения на редактирование модуля Zemez category!';
$_['error_name']       = 'Название модуля должно содержать от 3х до 64х символов!';
<?php
// Heading
$_['heading_title']    = 'Zemez Category';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Zemez category module!';
$_['text_edit']        = 'Edit Zemez Category Module';

// Entry
$_['entry_status']     = 'Status';
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Zemez category module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
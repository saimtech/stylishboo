<?php
class ControllerExtensionModuleZemezInstagram extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/zemez_instagram');
		$this->document->addScript('catalog/view/theme/' . $this->config->get('theme_' . $this->config->get('config_theme') . '_directory') . '/js/zemez_instagram/instafeed.min.js');

		$data['heading_instagram_title'] = $this->language->get('heading_instagram_title');
		$data['_get']           = $setting['_get'];
		$data['user_id']       = $setting['user_id'];
		$data['accesstoken']   = $setting['accesstoken'];
		$data['limit']         = $setting['limit'];

		return $this->load->view('extension/module/zemez_instagram', $data);
	}
}
<?php
// Heading
$_['heading_single_tabs_title']       = 'ZEMEZ Modul Tabs';
$_['heading_latest']      = 'Neuankömmlinge';
$_['heading_specials']    = 'Besonderheiten';
$_['heading_featured']    = 'Angesagt';
$_['heading_bestsellers'] = 'Bestseller';

// Text
$_['text_tax']            = 'Ohne Steuern:';
$_['text_sale']           = 'Verkauf!';
$_['text_new']            = 'Neu!';
$_['text_view']           = 'Alle Produkte anzeigen';
$_['text_option']         = 'Verfügbare Optionen';
$_['text_select']         = '--- Bitte auswählen ---';

$_['text_category']       = 'Kategorien';
$_['text_manufacturer']   = 'Marke:';
$_['text_model']          = 'Modell:';
$_['text_availability']   = 'Verfügbarkeit:';
$_['text_instock']        = 'Auf Lager';
$_['text_outstock']       = 'Ausverkauft';
$_['text_price']          = 'Preis: ';
$_['text_tax']            = 'Ohne Steuern:';
$_['text_quick']          = 'Ohne Steuern';
$_['text_save']           = 'sparen';
$_['button_details']      = 'Einzelheiten';
$_['reviews']             = 'Bewertungen ';
$_['text_product']        = 'Produkt {current} von {total} ';

<?php
// Heading
$_['text_newsletter']       = 'Newsletter';

// Entry
$_['entry_mail']          = 'Ihre E-Mail-Adresse';

// Text
$_['button_subscribe']    = 'Jetzt abonnieren';
$_['text_success']        = 'Sie haben erfolgreich abonniert';

//Errors
$_['error_exist_user']    = 'Benutzer bereits registriert';
$_['error_exist_email']   = 'Die angegebene E-Mail ist bereits abonniert';
$_['error_invalid_email'] = 'Die angegebene E-Mail ist bereits abonniert';